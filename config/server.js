var http = require('http');
var https = require('https');
var fs = require('fs');
var path = require('path');
var pug = require('pug');
var bodyParser = require('body-parser');

var passport = require('passport');
var BasicStrategy = require("passport-http").BasicStrategy;
var bcrypt = require('bcrypt');
var db = require('../common/mongodb.js');

//TODO: review

module.exports.set = function(app) {

  app.all('*', ensureSecure);

  app.set('views', [
    path.join(__dirname, '../member/views'),
    path.join(__dirname, '../admin/views'),
    ]);
  app.set('view engine', 'pug');

  app.use(bodyParser.json());
    app.use(bodyParser.urlencoded({ extended: true }));

    if (process.env.IP_FILTER === 'true') {
    var ipfilter = require('express-ipfilter').IpFilter;
    var ips = process.env.WHITELISTED_IPS.split(',');
    app.use(ipfilter(ips, {mode: 'allow'}));
  }

  db.admins.count(function(err, count) {
    if (err !== null) {
      console.log(err);
    } if (count === 0) {
      bcrypt.hash(process.env.ADMIN_PASSWORD, 10, function(err2, hash) {
        var admin = { username : process.env.ADMIN_USERNAME, password : hash };
        db.admins.insert(admin);
      });
    }
  });

  //TODO:
  passport.use(new BasicStrategy(
    function(username, password, done) {

      var query = { username : username };
      db.admins.findOne(query, function(err, admin) {

        if (admin !== null) {
          bcrypt.compare(password, admin.password).then(function(res) {
            if (res) {
              return done(null, { _id : admin._id, username: username });
            } else {
              return done();
            }
        });
        } else {
          return done();
        }
      });
    }
  ));

//TODO:
  passport.serializeUser(function(user, done) {
    done(null, user);
  });
//TODO:
  passport.deserializeUser(function(user, done) {
    done(null, user);
  });

  app.use(passport.initialize());

  http.createServer(app).listen(process.env.HTTP_PORT);
  https.createServer({
      key: fs.readFileSync(process.env.KEY_PATH),
      cert: fs.readFileSync(process.env.CRT_PATH),
      ca: fs.readFileSync(process.env.CA_PATH)
  }, app).listen(process.env.PORT);
}

function ensureSecure(req, res, next){
  if(req.secure){
    return next();
  };
  res.redirect('https://www.' + process.env.DOMAIN + req.url);
}
