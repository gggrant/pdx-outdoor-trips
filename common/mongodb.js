var MongoClient = require('mongodb').MongoClient;
var ObjectID = require('mongodb').ObjectID;

var ex = {};

ex.LOG_TYPE_DB_ERROR = 'dbError';
ex.LOG_TYPE_INSERT_MEMBER = 'insertMember';
ex.LOG_TYPE_UPDATE_MEMBER = 'updateMember';
ex.LOG_TYPE_EMAIL_ERROR = 'emailError';
ex.LOG_TYPE_ANNOUNCE_EMAIL = 'announceEmail';
ex.LOG_TYPE_SUPPORT_EMAIL = 'supportEmail';

ex.EMAIL_TYPE_ANNOUNCE = 'announce';
ex.EMAIL_TYPE_SUPPORT_INCOMING = 'supportIncoming';
ex.EMAIL_TYPE_SUPPORT_OUTGOING = 'supportGoing';

ex.init = function(callback) {

  var url = 'mongodb://' + process.env.MONGO_HOST + '/' + process.env.DB_NAME;

  MongoClient.connect(url, function(err1, openedDb) {

    ex.db = openedDb;

    ex.logs = openedDb.collection('logs');
    ex.admins = openedDb.collection('admins');
    ex.members = openedDb.collection('members');
    ex.emails = openedDb.collection('emails');

    ex.admins.ensureIndex({ username : 1 }, { unique : true }, function(err2, result2) {
      ex.members.ensureIndex({ email : 1 }, { unique : true }, function(err3, result3) {
        var err = (err1 !== null) ? err1 : ((err2 !== null) ? err2 : err3);
        callback(err, openedDb);
      });
    });
  });
}

ex.logDbInsertMember = function(req, data) {
  ex.insertLog(req, ex.LOG_TYPE_INSERT_MEMBER, data);
}

ex.logDbUpdateMember = function(req, data) {
  ex.insertLog(req, ex.LOG_TYPE_UPDATE_MEMBER, data);
}

ex.logDbError = function(err, req, data) {
  return module.exports.logError(err, req, module.exports.LOG_TYPE_DB_ERROR, data);
}

ex.logEmailError = function(err, req, data) {
  return module.exports.logError(err, req, module.exports.LOG_TYPE_EMAIL_ERROR, data);
}

ex.logError = function(err, req, type, data) {

  if (err !== undefined && err !== null) {

    if (data == null) { 
      data = {};
    }
    data.err = err;

    ex.insertLog(req, type, data);
  }

  return (err !== undefined && err !== null);
}

ex.insertLog = function(req, type, data) {

  if (data == null) { 
      data = {};
  }

  data.type = type;
  data.ip = req.ip;
  data.url = req.url;

  ex.db.collection('logs').insertOne(data, function(err) {
    if (err !== undefined && err !== null) {
      console.log(err);
    }
  });
}

module.exports = ex;
