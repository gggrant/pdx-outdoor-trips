var controller = require('./controller.js');

module.exports.set = function(app) {

  app.get('/', function (req, res) {
    controller.renderIndex(req, res);
  });

  app.get('/settings', function (req, res) {
    controller.renderSettings(req, res);
  });

  app.post('/settings', function (req, res) {
    controller.renderSettingsPost(req, res);
  });

  app.get('/confirm/:nonce', function (req, res) {
    controller.renderConfirm(req, res);
  });

};