
var db = require('../common/mongodb.js');
var ObjectID = require('mongodb').ObjectID;
var nonce = require('../common/nonce.js');
var mailgun = require('../common/mailgun.js');
var pug = require('pug');

module.exports.renderIndex = function(req, res) {
  res.render('index');
}

module.exports.renderSettings = function(req, res) {
  res.render('settings');
}

module.exports.renderSettingsPost = function(req, res) {

  var memberActions = {
    deactivate : { inactive : true },
    subscribe : { subscribed : true },
    unsubscribe : { subscribed : false }
  };

  if (Object.keys(memberActions).indexOf(req.body.action) >= 0) {

    var action = memberActions[req.body.action];

    var urlNonce = nonce.generate(64);
    
    var options = {
      $set : {
        pendingSettings : {
          settings : action,
          nonce : urlNonce
        }
      }
    };

    req.body.email = req.body.email.toLowerCase();

    var query = { email : req.body.email };

    db.members.findOneAndUpdate(query, options, function(err, result) {

      if (!db.logDbError(err, req, { body : req.body })) {

        if (result.value !== null) { 

          db.logDbUpdateMember(req, { 
            memberId : new ObjectID(result.value._id),
            email : result.value.email,
            query : query,
            options : { set : options.$set }
          });

          var locals = {
            nonceLink : 'https://www.' + process.env.DOMAIN + '/confirm/' + urlNonce,
            action : action
          };

          var data = {
            from : process.env.NOREPLY_EMAIL,
            to : req.body.email,
            subject : 'Please confirm your settings',
            html : pug.renderFile(__dirname + '/views/settings-confirm-html.pug', locals),
            text : pug.renderFile(__dirname + '/views/settings-confirm-text.pug', locals)
          };
          
          mailgun.messages().send(data, function (err2, body) {
            db.logEmailError(err2, req, { body : req.body, res : body });
          });
        }
      }
    });
  }

  res.render('settings-post');
}

module.exports.renderConfirm = function(req, res) {

  var query = {
    'pendingSettings.nonce' : req.params.nonce
  };

  db.members.findOne(query, function(err, doc) {
  
    if (db.logDbError(err, req, { body : req.body })) {
      res.sendStatus(500);

    } else if (doc === null) {
      res.render('expired');
  
    } else {
      var options = {
        $set : {
          pendingSettings : {}
        }
      };

      var settings = doc.pendingSettings.settings;
      var approvedSettings = ['blacklisted', 'inactive', 'subscribed'];

      for (var i in approvedSettings) {
        var setting = approvedSettings[i];
        if (settings[setting] !== undefined) {
          options.$set[setting] = settings[setting];
        }
      }

      db.members.findOneAndUpdate(
        query, options, function(err2, result) {
        if (!db.logDbError(err2, req, { body : req.body })) {
          db.logDbUpdateMember(req, { 
              memberId : new ObjectID(result.value._id),
              email : result.value.email,
              query : { pendingSettings : { nonce : req.params.nonce } },
              options : { set : options.$set }
          });
        }
      });

      res.render('confirm');
    }
  });
}