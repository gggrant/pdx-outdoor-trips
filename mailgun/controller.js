var db = require('../common/mongodb.js');
var ObjectID = require('mongodb').ObjectID;
var mailgun = require('../common/mailgun.js');
var pug = require('pug');

module.exports.receiveAnnounce = function(req, res) {

  res.sendStatus(200);

  var fromEmail = parseFromEmail(req.body);

  db.members.findOne({ 
    email : fromEmail, 
    blacklisted : false,
    inactive : false 
  }, function(err, member) {
    
    if (!db.logDbError(err, req, { body : req.body })) {

      // verify sender membership
      if (member !== null) {

        //TODO: subscribed = false?

        //TODO: should this be here? vs after e-mail sent successfully?
        insertAnnounceEmail(req);

        var selector = {
          blacklisted : false,
          inactive : false,
          subscribed : true,
          email : {
            $ne : fromEmail
          }
        };
        var options = {
          fields : {
            email : 1
          }
        };

        db.members.find(selector, options).toArray(function(err2, members) {

          if (!db.logDbError(err2, req, { body : req.body })) {

            var bccEmails = [];

            for (var i in members) {
                bccEmails.push(members[i].email);
            }

            var locals = {
              siteLink : 'https://www.' + process.env.DOMAIN,
              tripSubject : req.body.subject,
              from : req.body.from,
              originalBodyHtml : req.body['body-html'],
              originalBodyText : req.body['body-plain']
            };

            // send trip announcement
            mailgun.messages().send({
              from: process.env.NOREPLY_EMAIL,
              to: process.env.NOREPLY_EMAIL,
              "h:Reply-To" : req.body.from,
              bcc: bccEmails.join(),
              subject: 'Trip: ' + req.body.subject,
              html : pug.renderFile(__dirname + '/views/announce-forward-html.pug', locals),
              text : pug.renderFile(__dirname + '/views/announce-forward-text.pug', locals)   
            }, function (err3, body) {
              db.logEmailError(err3, req, { body : req.body });

              // send trip announcement confirmation
              mailgun.messages().send({
                from : process.env.NOREPLY_EMAIL,
                to : req.body.from,
                subject: 'Trip: ' + req.body.subject,
                html : pug.renderFile(__dirname + '/views/announce-confirm-html.pug', locals),
                text : pug.renderFile(__dirname + '/views/announce-confirm-text.pug', locals)   
              }, function (err4, body2) {
                db.logEmailError(err4, req, { body : req.body });
              });
            });
          }
        });
      }
    }
  });
}

module.exports.receiveSupport = function(req, res) {
  console.log(req.body);

  res.sendStatus(200);
}

var parseFromEmail = function(body) {
  var fromEmail = body['X-Envelope-From'];
  if (fromEmail.indexOf('<') === 0) {
    fromEmail = fromEmail.substring(1, fromEmail.length - 1);
  }
  return fromEmail.toLowerCase();
}

var insertAnnounceEmail = function(req) {
  db.emails.insert({
    type : db.EMAIL_TYPE_ANNOUNCE,
    mailgun : req.body
  }, function(err, result) {
    db.logDbError(err, req, { body : req.body });
  });
}