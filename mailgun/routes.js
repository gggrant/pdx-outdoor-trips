var controller = require('./controller.js');

module.exports.set = function(app) {

	var mailgunDir = '/mailgun';
	var receiveDir = mailgunDir + '/receive';

	app.post(receiveDir + '/announce', function (req, res) {
		controller.receiveAnnounce(req, res);
	});

	app.post(receiveDir + '/support', function (req, res) {
		controller.receiveSupport(req, res);
	});
}