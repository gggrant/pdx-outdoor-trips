require('dotenv').config();
var express = require('express');
var app = express();

require('./common/mongodb').init(function(err, db) {
  if (err === null) {
    require('./config').set(app);
    require('./admin').set(app);
    require('./member').set(app);
    require('./mailgun').set(app);
  }
});
