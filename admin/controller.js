var db = require('../common/mongodb.js');
var ObjectID = require('mongodb').ObjectID;
var mailgun = require('../common/mailgun.js');
var pug = require('pug');

module.exports.renderDash = function(req, res) {

  res.render('dash');
}

module.exports.renderMembersPost = function(req, res) {

  var memberActions = {
    blacklist : { blacklisted : true },
    unblacklist : { blacklisted : false },
    activate : { inactive : false },
    deactivate : { inactive : true },
    subscribe : { subscribed : true },
    unsubscribe : { subscribed : false }
  };

  // insert new member
  if (req.body.action === 'add') {
    var flags = req.body.flags;
    if (flags === undefined) {
      flags = [];
    }

    req.body.email = req.body.email.toLowerCase();

    var member = {
      email : req.body.email,
      blacklisted : (flags.indexOf('blacklisted') >= 0),
      inactive : (flags.indexOf('inactive') >= 0),
      subscribed : (flags.indexOf('subscribed') >= 0)
    };

    db.members.insert(member, function(err1, result) {

      var fields = {};

      if (db.logDbError(err1, req, { body : req.body })) {

        fields.errorMessage = err1.message;

      } else {

        db.logDbInsertMember(req, { body : req.body });

        var locals = {
          siteLink : 'https://www.' + process.env.DOMAIN,
        };

        var data = {
          from : process.env.NOREPLY_EMAIL,
          to : req.body.email,
          subject : 'Welcome to PDX Outdoor Trips',
          html : pug.renderFile(__dirname + '/views/welcome-html.pug', locals),
          text : pug.renderFile(__dirname + '/views/welcome-text.pug', locals)
        };
        
        mailgun.messages().send(data, function (err2, body) {
          db.logEmailError(err2, req, { body : req.body, res : body });
        });

        fields.successMessage = member.email + ' added ' + flags.join(', ');
      } 

      module.exports.renderMembers(req, res, fields);
    });

  // update existing member
  } else if (Object.keys(memberActions).indexOf(req.body.action) >= 0) {

    var action = memberActions[req.body.action];

    var _id = new ObjectID(req.body.id);

    db.members.findOneAndUpdate(
      { _id : _id }, 
      { $set : action }, function(err1, result) {

      var fields = {};

      if (db.logDbError(err1, req, { body : req.body })) {

        fields.errorMessage = err1.message;

      } else {

        db.logDbUpdateMember(req, { body : req.body });

        fields.successMessage = result.value.email + ' updated ' + 
          Object.keys(action)[0] + ' = ' +
          action[Object.keys(action)[0]];
      }

      module.exports.renderMembers(req, res, fields);
    });
  } else {
    //TODO: invalid action
  } 
}

module.exports.renderMembers = function(req, res, fields) {

  var memberFilters = {
    all : { },
    active : { blacklisted : false, inactive : false },
    inactive : { blacklisted : false, inactive : true,  },
    subscribed : { blacklisted : false, inactive : false, subscribed : true },
    unsubscribed : { blacklisted : false, inactive : false, subscribed : false },
    blacklisted : { blacklisted : true }
  };

  var memberOrders = {
    created : '_id', 
    email : 'email'
  };

  var pageSize = 50;

  var filter = req.params.filter;
  if (Object.keys(memberFilters).indexOf(filter) < 0) {
    Object.keys(memberFilters)[0];
  }

  var selector = memberFilters[filter];

  var page = req.params.page;
  if (page === undefined || isNaN(page)) {
    page = 0;
  }

  var limit = pageSize;
  var skip = page*pageSize;

  var order = memberOrders[req.params.order];
  if (order === undefined) {
    order = '_id';
  }

  var direction = parseInt(req.params.direction);
  if (direction !== 1 && direction !== -1) {
    direction = 1;
  }

  var sort = {};
  sort[order] = direction;

  db.members.find(selector, {
    limit : limit,
    skip : skip,
    sort : sort
  }).toArray(function(err, members) {

    members.forEach(function(member){
      member.creationDate = new Date(new ObjectID(member._id).getTimestamp()).toISOString().substring(0, 10);
        member.detailUrl = '/admin/member/' + member.email;
    });

    if (fields === undefined) {
      fields = {};
    }

    var heading = 'Members ' + filter + ' page ' + page + ' sorted by ' + order;

      res.render('members', Object.assign({
        heading: heading,
      filter: filter,
      page: page, 
      members: members
    }, fields));
  }); 
}

module.exports.renderMember = function(req, res, options) {
  res.render('member');
}

module.exports.renderEmails = function(req, res, options) {

  var pageSize = 50;

  db.emails.find({}, {
    limit: pageSize,
    sort: { _id : -1 }
  }).toArray(function(err, emails) {

    for (var i in emails) {
      emails[i].message = JSON.stringify(emails[i]);
    }

    res.render('emails', {
      emails : emails
    });
  });
}

module.exports.renderEmail = function(req, res, options) {
  res.render('email');
}

module.exports.renderLogs = function(req, res, options) {

  var pageSize = 50;

  db.logs.find({}, {
    limit: pageSize,
    sort: { _id : -1 }
  }).toArray(function(err, logs) {

    for (var i in logs) {
      logs[i].message = JSON.stringify(logs[i]);
    }

    res.render('logs', {
      logs: logs
    });
  });
}






