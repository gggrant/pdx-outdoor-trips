var controller = require('./controller.js');
var passport = require('passport');

module.exports.set = function(app) {

  var adminDir = '/admin';

  app.get(adminDir, passport.authenticate('basic'), function (req, res) {
    res.redirect(adminDir + '/dash');
  });

  app.get(adminDir + '/dash', passport.authenticate('basic'), function (req, res) {
    controller.renderDash(req, res);
  });

  app.get(adminDir + '/members', passport.authenticate('basic'), function (req, res) {
    res.redirect(adminDir + '/members/all/0/created/-1');
  });

  app.get(adminDir + '/members/:filter/:page/:order/:direction', passport.authenticate('basic'), function (req, res) {
    controller.renderMembers(req, res);
  });

  app.post(adminDir + '/members/:filter/:page/:order/:direction', passport.authenticate('basic'), function (req, res) {
    controller.renderMembersPost(req, res);
  });

  app.get(adminDir + '/member/:email', passport.authenticate('basic'), function (req, res) {
    controller.renderMember(req, res);
  });

  app.get(adminDir + '/emails', passport.authenticate('basic'), function (req, res) {
    res.redirect(adminDir + '/emails/all/0/sent/-1');
  });

  app.get(adminDir + '/emails/:filter/:page/:order/:direction', passport.authenticate('basic'), function (req, res) {
    controller.renderEmails(req, res);
  });

  app.get(adminDir + '/email/:id', passport.authenticate('basic'), function (req, res) {
    controller.renderEmail(req, res);
  });

  app.get(adminDir + '/logs', passport.authenticate('basic'), function (req, res) {
    res.redirect(adminDir + '/logs/all/0/sent/-1');
  });

  app.get(adminDir + '/logs/:filter/:page/:order/:direction', passport.authenticate('basic'), function (req, res) {
    controller.renderLogs(req, res);
  });

  //TODO: this doesn't do anything
  app.get(adminDir + '/logout', passport.authenticate('basic'), function(req, res){
    req.logout();
    req.session = {};
    res.redirect(adminDir);
  });

}